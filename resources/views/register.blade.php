<!DOCTYPE html>
<html>

<head>
    <title>Tugas JCC - Form</title>
</head>

<body>
    <form action="{{url('welcome')}}" method="POST">
        @csrf
        <h1>Buat Account Baru</h1>
        <h2>Sign Up Form</h2>

        <p>First Name:</p>
        <input type="text" name="first-name" />
        <br />

        <p>Last Name:</p>
        <input type="text" name="last-name" />
        <br />

        <p>Gender:</p>
        <input type="radio" value="male" name="gender" />Male<br />
        <input type="radio" value="female" name="gender" />Female<br />
        <input type="radio" value="other" name="gender" />Other<br />

        <p>Nationality:</p>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporian</option>
            <option value="malaysian">Malaysian</option>
            <option value="american">American</option>
        </select>
        <br />

        <p>Language Spoken:</p>
        <input type="checkbox" name="language-spoken" value="indonesia">Bahasa indonesia<br />
        <input type="checkbox" name="language-spoken" value="english">English<br />
        <input type="checkbox" name="language-spoken" value="other">Other<br />

        <p>Bio:</p>
        <textarea cols="30" rows="10"></textarea>
        <br />

        <button type="submit">Sign Up</button>
    </form>
</body>

</html>