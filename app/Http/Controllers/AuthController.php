<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        $post = $request->all();
        $name = $post['first-name'].' '.$post['last-name'];
        $data = [
            'name' => $name
        ];
        return view('welcome',$data);
    }
}
